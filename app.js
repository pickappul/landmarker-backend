'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('winston');
const passport = require('passport');

const config = require('./config.json');
const Model = require('./app/model');
const Controllers = require('./app/controllers');
const routes = require('./config/routes');

var model = Model(config);
var controllers = Controllers(model);
var app = express();

// Authentication
passport.use(controllers.auth.Strategy);
passport.serializeUser(controllers.auth.serializeUser);
passport.deserializeUser(controllers.auth.deserializeUser);
app.use(passport.initialize());
app.use(passport.session());

// Middleware
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Setup routes
//routes(app, controllers);
app.post('/login', passport.authenticate('local'), function(req, res) {
    res.redirect('/');
});

// Listen
app.listen(3000, function(){
    logger.info('[App]', 'listening at port 3000!');
});
