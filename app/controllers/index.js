function Controllers(model) {
    const controllers = Object.freeze({
        auth: require('./auth')(model)
    });
    return controllers;
}

module.exports = Controllers;