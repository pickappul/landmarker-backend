'use strict';

/**
 * Module dependencies
 */
const LocalStrategy = require('passport-local').Strategy;
const LoginError = require('../model/errors').LoginError;

var _model;

var Strategy = new LocalStrategy(function (username, password, done) {
    _model.User.login({ username: username, password: password }).then((user) => {
        done(null, user);
    }).catch(LoginError, function(err) {
        done(err);
    });
});

function serializeUser(user, done) {
    done(null, user._id);
}

function deserializeUser(uid, done) {
    _model.User.findByUid(uid).then((user) => {
        done(null, user);
    }, (err) => {
        done(err);
    });
}

function AuthController(model) {
    _model = model;
    
    var authController = Object.freeze({
        Strategy: Strategy,
        serializeUser: serializeUser,
        deserializeUser: deserializeUser
    });
    return authController;
}

module.exports = AuthController;