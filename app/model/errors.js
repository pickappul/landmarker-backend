// This file is used to define custom error types required by other modules

function LoginError(message) {
    this.name = 'LoginError';
    this.message = (message || '');
    this.stack = new Error().stack;
}
LoginError.prototype = Object.create(Error.prototype);

module.exports = {
    LoginError: LoginError
};