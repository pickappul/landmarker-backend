'use strict';

/**
 * Module depedencies
 */
const Promise = require('bluebird');
const bcrypt = require('bcryptjs');
const uuid = require('node-uuid');
const LoginError = require('./errors').LoginError;

var UserModel;

function _hashPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, function(err, hash) {
                if (err) reject(err);
                else resolve(hash);
            });
        });
    });
}

/**
 * Static methods
 */

function exists(user) {
    return UserModel.find(user).count().lean().exec();
}

function findByUid(uid) {
    return UserModel.findOne({ _id: uid }).exec();
}

function findByUsername(username) {
    return UserModel.findOne({ username: username }).exec();
}

/* 
 * Used to validate if a user password combination exists.
 * The user argument has a password property containing an unhashed password.
 * Returns the User document in storage if resolved.
 * @param {Object} user
 */
function login(user) {
    return new Promise((resolve, reject) => {
        UserModel.findOne({ username: user.username }).lean().exec((err, dbUser) => {
            if (err) reject(err);
            else if (!dbUser) reject(new LoginError("Incorrect username"));
            else bcrypt.compare(user.password, dbUser.password, function(err, res) {
                if (err) reject(err);
                // TODO: If this message passes through it should be less specific.
                else if (!res) reject(new LoginError("Incorrect password"));
                else resolve(dbUser);
            });
        });
    });
}

/**
 * Instance methods
 */

function checkInstancePassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.password, (err, res) => {
            resolve(res);
        });
    });
}

function hashInstancePassword() {
    return new Promise((resolve, reject) => {
        _hashPassword(this.password).then((hash) => {
            this.password = hash;
            resolve();
        });
    });
}

// Constructor, mongoose injected
function User(mongoose) {
    const Schema = mongoose.Schema;
    
    /**
     * User schema
     */
    const userSchema = new mongoose.Schema({
        _id: String,
        username: String,
        password: String
    });

    userSchema.statics = {
        exists: exists,
        findByUid: findByUid,
        findByUsername: findByUsername,
        login: login
    };

    userSchema.methods = {
        hashPassword: hashInstancePassword,
        checkPassword: checkInstancePassword
    };

    UserModel = mongoose.model('User', userSchema);
    return UserModel;
}

module.exports = User;