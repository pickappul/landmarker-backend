'use strict';

var Mongoose = require('mongoose').Mongoose;
var mongoose = new Mongoose();

const Promise = require('bluebird');
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const sinon = require('sinon');
const sinonTest = require('sinon-test');
const mockgoose = require('mockgoose');
const logger = require('winston');

chai.use(chaiAsPromised);
const assert = chai.assert;
sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

require('sinon-as-promised')(Promise);
require('sinon-mongoose');

const Model = require('../../app/model');
const LoginError = require('../../app/model/errors').LoginError;

var model;
const testConfig = Object.freeze({
    db: {
        uri: "mongodb://localhost/test"
    }
});

describe('User Model', function() {

    before(function(done) {
        logger.remove(logger.transports.Console); // Suppress logging
        mockgoose(mongoose).then(() => {
            model = Model(testConfig, mongoose, () => {
                done();
            });
        });
    });

    describe('#hashPassword(), #checkPassword()', function() {
        it('should modify a User instance password', function() {
            const testPassword = 'testing';
            var testUser = new model.User({
                username: 'test',
                password: testPassword
            });
            testUser.hashPassword().then(() => {
                assert(testUser.password !== testPassword);
            });
        });

        it('should leave a valid hash', function() {
            const testPassword = 'testing';
            var testUser = new model.User({
                username: 'test',
                password: testPassword
            });
            testUser.hashPassword().then(assert(testUser.checkPassword(testPassword)));
        });
    });

    describe('#login()', function() {
        it('should resolve to an instance of the logged in user', sinon.test(function() {
            // Mock a User
            this.mock(model.User)
                .expects('find').withArgs({ username: 'USERNAME' })
                .chain('lean')
                .chain('exec')
                .yields(null, new model.User({
                    username: 'USERNAME',
                    password: '$2a$10$plrHzNtn2mHX8sP0rzxN0e0kFU9ih2f/pXuk.Hu0looZh3YDn/EQm'
                }));

            var promise = model.User.login({
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            return assert.eventually.deepPropertyVal(promise, 'constructor.modelName', 'User');         
        }));

        it('should throw a LoginError if the user/password is incorrect', sinon.test(function() {
            // Mock an empty response
            this.mock(model.User)
                .expects('find')
                .chain('lean')
                .chain('exec')
                .yields(null, null);

            var promise = model.User.login({
                username: 'ANOTHER',
                password: 'PASSWORD'
            });
            return assert.isRejected(promise, LoginError);
        }));
    });

    /*describe('#count()', function() {
        it('should return 0 if the user does not exist', function() {
            assert.equal(0, )
        });
    });*/
});