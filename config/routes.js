'use strict';

function setupRoutes(app, controllers) {
    app.post('/login', passport.authenticate('local', { failureRedirect: '/login' }),
        function(req, res) {
            res.redirect('/');
        });
}

module.exports = setupRoutes;